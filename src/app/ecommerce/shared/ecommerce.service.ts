import { Category } from './category';
import { Injectable } from '@angular/core';
import { Product } from './product';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EcommerceService {

  rootUrl: string = "http://localhost:53878/api/";
  staticProductList: Product[] = [];
  productList: Product[] = [];
  constructor(private http: HttpClient) {
    this.setAll();
  }

  getAll(): Observable<Product[]> {
    return this.http.get<Product[]>(this.rootUrl + "Products");
  }
  setAll() {
    this.getAll().subscribe((data) => {
      this.staticProductList = data;
    },
      (error) => {
        this.staticProductList = [];
      })
  }

  setAllByCategoryId(id: number) {
    this.productList = this.staticProductList.filter(data => data.CategoryId == id);
  }
}
