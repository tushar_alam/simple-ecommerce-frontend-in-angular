import { Category } from './category';

export class Product {
    Id:number;
    Name:string;
    BrandName:string;
    Description:string;
    CategoryId:number;
    Category:Category;
    ImagePath:string;
}
