import { EcommerceService } from './../shared/ecommerce.service';
import { Product } from './../shared/product';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  listChangedSubscribe: Subscription;

  constructor(private service: EcommerceService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
   
  }

  getProduct(id: number) {
    this.router.navigate(['/productDetail', id]);
  }
}
