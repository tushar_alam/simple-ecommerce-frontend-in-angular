import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Category } from '../shared/category';
import { EcommerceService } from '../shared/ecommerce.service';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/categories/shared/category.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  listChangedSubscribe: Subscription;
  categoryList: Category[] = [];
  

  constructor(private service: EcommerceService,
    private categoryService: CategoryService,
    private router: Router) { }

  ngOnInit() {
    this.categoryService.setAll();
    this.listChangedSubscribe = this.categoryService.listChanged.subscribe((data) => { this.categoryList = data},(error) => {
      this.categoryList = [];
    });
  }

  getProductList(id: number) {
    this.router.navigate(['/productList']);
    this.service.setAllByCategoryId(id);
  }

}
