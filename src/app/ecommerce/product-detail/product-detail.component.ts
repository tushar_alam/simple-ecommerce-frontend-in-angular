import { Product } from './../shared/product';
import { Component, OnInit } from '@angular/core';
import { EcommerceService } from '../shared/ecommerce.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product: Product;
  constructor(private service: EcommerceService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.params['id'];
    if (id != null) {
      this.product = this.service.staticProductList.find(product => product.Id == id);
    }
  }

}
