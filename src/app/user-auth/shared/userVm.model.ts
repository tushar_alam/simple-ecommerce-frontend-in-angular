export class UserVm {
    Id: number;
    UserName: string;
    Password: string;
    Role: string;
    Token?: string;
    Image:string;
}
