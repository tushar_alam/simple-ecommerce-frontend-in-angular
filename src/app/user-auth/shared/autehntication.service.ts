import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserVm } from './userVm.model';

@Injectable({
  providedIn: 'root'
})
export class AutehnticationService {
  private currentUserSubject: BehaviorSubject<UserVm>;
  public currentUser: Observable<UserVm>;

  constructor(private http: HttpClient) {
  }

  public get currentUserValue(): UserVm {
    return this.currentUserSubject.value;
  }

  login(UserName: string, Password: string) {

  }

  logout() {

  }
}
