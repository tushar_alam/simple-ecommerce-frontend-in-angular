import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router"


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  returnUrl: string;
  hide = true;
  isLoginError: boolean = false;
  
  constructor(private router: Router,
    private route: ActivatedRoute) {

  }

  ngOnInit() {
  }

  onSubmit(form?:NgForm) {

  }
}
