import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http'; 
 
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgxPaginationModule} from 'ngx-pagination';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SignInComponent } from './user-auth/sign-in/sign-in.component';
import { UserAuthComponent } from './user-auth/user-auth.component';

import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { DashboardComponent } from './ecommerce/dashboard/dashboard.component';
import { CategoryListComponent } from './ecommerce/category-list/category-list.component';
import { ProductListComponent } from './ecommerce/product-list/product-list.component';
import { ProductDetailComponent } from './ecommerce/product-detail/product-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductsComponent } from './products/products.component';
import { ProductTableComponent } from './products/product-table/product-table.component';
import { ProductComponent } from './products/product/product.component';
import { InquiriesComponent } from './inquiries/inquiries.component';
import { InquiryTableComponent } from './inquiries/inquiry-table/inquiry-table.component';
import { InquiryComponent } from './inquiries/inquiry/inquiry.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryComponent } from './categories/category/category.component';
import { CategoryTableComponent } from './categories/category-table/category-table.component';
import { InquiryDetailComponent } from './inquiries/inquiry-detail/inquiry-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    EcommerceComponent,
    DashboardComponent,
    CategoryListComponent,
    ProductListComponent,
    ProductDetailComponent,
    UserAuthComponent,
    SignInComponent,
    PageNotFoundComponent,
    ProductsComponent,
    ProductTableComponent,
    ProductComponent,
    InquiriesComponent,
    InquiryTableComponent,
    InquiryComponent,
    CategoriesComponent,
    CategoryComponent,
    CategoryTableComponent,
    InquiryDetailComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    NgxPaginationModule,
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
