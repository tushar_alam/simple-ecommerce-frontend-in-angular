import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Inquiry } from './inquiry';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InquiryService {
  rootUrl:string="http://localhost:53878/api/";

  startedEditing = new Subject<number>();
  listChanged = new Subject<Inquiry[]>();
  deleted=new Subject<boolean>();

  inquiries: Inquiry[] = [];
  inquiry: Inquiry;
  constructor(private http: HttpClient) { }

  add(entity: Inquiry): Observable<Inquiry> {
    return this.http.post<Inquiry>(this.rootUrl + "Inquries/PostInqury", entity);
  }

  update(id: number, entity: Inquiry): Observable<any> {
    entity.Id=id;
    return this.http.post(this.rootUrl + "Inquries/EditInqury", entity);
  }

  delete(id: number): Observable<Inquiry> {
    return this.http.delete<Inquiry>(this.rootUrl + "Inquries" + "/" + id);
  }

  getById(id: number): Observable<Inquiry> {
    return this.http.get<Inquiry>(this.rootUrl + "Inquries" + "/" + id);
  }

  getByIdForRead(id: number): Observable<Inquiry> {
    return this.http.get<Inquiry>(this.rootUrl + "Inquries/ForRead" + "/" + id);
  }

  getAll(): Observable<Inquiry[]> {
    return this.http.get<Inquiry[]>(this.rootUrl + "Inquries");
  }

  setById(id: number) {
    this.getById(id).subscribe((data) => {
      this.inquiry = data;
    },
      (error) => {
        this.inquiry = {
          Id: null,
          Subject: '',
          Email: '',
          Message:'',
          Status:null,
        }
      })
  }

  setAll() {
    this.getAll().subscribe((data) => {
      this.inquiries = data;
      this.listChanged.next(this.inquiries.slice())
    },
      (error) => {
        this.inquiries = [];
      })
  }
}
