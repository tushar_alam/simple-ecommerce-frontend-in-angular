import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Inquiry } from '../shared/inquiry';
import { InquiryService } from '../shared/inquiry.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Status } from '../shared/status';

@Component({
  selector: 'app-inquiry',
  templateUrl: './inquiry.component.html',
  styleUrls: ['./inquiry.component.css']
})
export class InquiryComponent implements OnInit {

  @ViewChild('form', null) form: NgForm;

  InitialStatusId = 1;
  statusList: Status[] = [
    {
      Id: 1,
      Name: 'Unread'
    },
    {
      Id: 2,
      Name: 'Read'
    }];

  editMode = false;
  editedInquiryId: number;
  editedInquiry: Inquiry;


  constructor(private inquiryService: InquiryService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.reset();
    this.setDataByParams();
  }

  onSubmit(form: NgForm) {
    const inquiry = form.value;
    inquiry.Status = 1;

    if (this.editMode) {
      this.inquiryService.update(this.editedInquiryId, inquiry).subscribe((data) => {
        this.toastr.success('Update Successfull', 'Success');
        this.router.navigate(['/adminLogin/inquiry-list']);
      },
        (error) => {
          this.toastr.error('Update Faild', 'Error');
        })
    } else {
      this.inquiryService.add(inquiry).subscribe((data) => {
        this.toastr.success('Save Successfull', 'Success');
        this.router.navigate(['/adminLogin/inquiry-list']);
      },
        (error) => {
          this.toastr.error('Save Faild', 'Error');
        })
    }
    this.reset(form);
  }

  reset(form?: NgForm) {
    if (form != null && form != undefined) {
      form.reset();
    } else {
      this.form.reset();
    }
    this.editMode = false;
  }

  setDataByParams() {
    let id = this.activatedRoute.snapshot.params['id'];
    if (id != null) {
      this.inquiryService.getById(id).subscribe(data => {
        this.editMode = true;
        this.editedInquiryId = id;
        this.editedInquiry = data;
        this.form.setValue({
          Subject: this.editedInquiry.Subject,
          Email: this.editedInquiry.Email,
          Message: this.editedInquiry.Message,
          Status:this.editedInquiry.Status
        })
      });
    }
  }



}
