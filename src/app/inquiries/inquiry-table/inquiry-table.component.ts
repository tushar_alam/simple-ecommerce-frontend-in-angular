import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Inquiry } from '../shared/inquiry';
import { InquiryService } from '../shared/inquiry.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inquiry-table',
  templateUrl: './inquiry-table.component.html',
  styleUrls: ['./inquiry-table.component.css']
})
export class InquiryTableComponent implements OnInit {

  p: number = 1;
  listChangedSubscribe: Subscription;
  confirmSubscription: Subscription;

  inquiryList: Inquiry[] = [];

  constructor(private inquiryService: InquiryService,
    private toastr: ToastrService,
    private router: Router, ) { }

  ngOnInit() {
    this.inquiryService.setAll();
    this.listChangedSubscribe = this.inquiryService.listChanged.subscribe((data) => { this.inquiryList = data }, (error) => {
      this.inquiryList = [];
    });

    this.confirmSubscription = this.inquiryService.deleted.subscribe((data) => {
      if (data) {
        this.inquiryService.setAll();
        this.toastr.warning('Deleted Successfully', 'Success');
      }
    });
  }


  onDelete(Id: number) {
    this.inquiryService.delete(Id).subscribe((data) => {
      this.inquiryService.deleted.next(true);
    }, (error) => {
      this.inquiryService.deleted.next(false);
    });
  }

  getStatus(status: number): string {
    if (status === 1) {
      return "Unread";
    } else {
      return "Read";
    }
  }

  onEdit(Id: number) {
    this.router.navigate(['/adminLogin/inquery-edit', Id]);
  }

  onDetail(Id: number) {
    this.router.navigate(['/adminLogin/inquery-detail', Id]);
  }

  ngOnDestroy() {
    this.confirmSubscription.unsubscribe();
    this.listChangedSubscribe.unsubscribe();
  }

}
