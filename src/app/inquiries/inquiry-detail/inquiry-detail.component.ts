import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InquiryService } from '../shared/inquiry.service';
import { Inquiry } from '../shared/inquiry';

@Component({
  selector: 'app-inquiry-detail',
  templateUrl: './inquiry-detail.component.html',
  styleUrls: ['./inquiry-detail.component.css']
})
export class InquiryDetailComponent implements OnInit {

  editedInquiry: Inquiry;

  constructor(private inquiryService: InquiryService,
    private activatedRoute: ActivatedRoute,
    private router: Router, ) { }

  ngOnInit() {
    this.setDataByParams();
  }

  setDataByParams() {
    let id = this.activatedRoute.snapshot.params['id'];
    if (id != null) {
      this.inquiryService.getByIdForRead(id).subscribe(data => {
        this.editedInquiry = data;
      });
    }
  }
}
