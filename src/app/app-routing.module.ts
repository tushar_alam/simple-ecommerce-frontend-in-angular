import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAuthComponent } from './user-auth/user-auth.component';
import { SignInComponent } from './user-auth/sign-in/sign-in.component';
import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { DashboardComponent } from './ecommerce/dashboard/dashboard.component';
import { CategoryListComponent } from './ecommerce/category-list/category-list.component';
import { ProductListComponent } from './ecommerce/product-list/product-list.component';
import { ProductDetailComponent } from './ecommerce/product-detail/product-detail.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductsComponent } from './products/products.component';
import { InquiriesComponent } from './inquiries/inquiries.component';
import { InquiryComponent } from './inquiries/inquiry/inquiry.component';
import { InquiryTableComponent } from './inquiries/inquiry-table/inquiry-table.component';
import { InquiryDetailComponent } from './inquiries/inquiry-detail/inquiry-detail.component';



const routes: Routes = [
  {
    path: '', component: EcommerceComponent,
    children: [
      { path: '', component: DashboardComponent },
      { path: 'categoryList', component: CategoryListComponent },
      { path: 'productList', component: ProductListComponent },
      { path: 'productDetail/:id', component: ProductDetailComponent },
    ]
  },
  {
    path: 'adminLogin', component: UserAuthComponent,
    children: [
      { path: 'inquiry', component: InquiryComponent },
      { path: 'inquiry-list', component: InquiryTableComponent },
      { path: 'inquery-detail/:id', component: InquiryDetailComponent },
      { path: 'inquery-edit/:id', component: InquiryComponent },

      { path: 'product', component: ProductsComponent },
      { path: 'signin', component: SignInComponent }
    ]
  },
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
