import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/ecommerce/shared/product';
import { ProductService } from '../shared/product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {

  p: number = 1;
  listChangedSubscribe: Subscription;
  confirmSubscription: Subscription;

  productList: Product[] = [];

  constructor(private productService: ProductService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.productService.setAll();
    this.listChangedSubscribe = this.productService.listChanged.subscribe((data) => { this.productList = data},(error) => {
        this.productList = [];
      });

    this.confirmSubscription = this.productService.deleted.subscribe((data) => {
      if (data) {
        this.productService.setAll();
        this.toastr.warning('Deleted Successfully', 'Success');
      }
    });
  }

  onEdit(Id: number) {
    this.productService.startedEditing.next(Id);
  }

  onDelete(Id: number) {
    this.productService.delete(Id).subscribe((data) => {
      this.productService.deleted.next(true);
    }, (error) => {
      this.productService.deleted.next(false);
    });
  }


  ngOnDestroy() {
    this.confirmSubscription.unsubscribe();
    this.listChangedSubscribe.unsubscribe();
  }


}
