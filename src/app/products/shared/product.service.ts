import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Product } from 'src/app/ecommerce/shared/product';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  rootUrl:string="http://localhost:53878/api/";

  startedEditing = new Subject<number>();
  listChanged = new Subject<Product[]>();
  deleted=new Subject<boolean>();

  products: Product[] = [];
  product: Product;
  constructor(private http: HttpClient) { }

  add(entity: Product): Observable<Product> {
    return this.http.post<Product>(this.rootUrl + "Products/PostProduct", entity);
  }

  update(id: number, entity: Product): Observable<any> {
    entity.Id=id;
    return this.http.post(this.rootUrl + "Products/EditProduct", entity);
  }

  delete(id: number): Observable<Product> {
    return this.http.delete<Product>(this.rootUrl + "Products" + "/" + id);
  }

  getById(id: number): Observable<Product> {
    return this.http.get<Product>(this.rootUrl + "Products" + "/" + id);
  }
  getAll(): Observable<Product[]> {
    return this.http.get<Product[]>(this.rootUrl + "Products");
  }

  setById(id: number) {
    this.getById(id).subscribe((data) => {
      this.product = data;
    },
      (error) => {
        this.product = {
          Id: null,
          Name: '',
          BrandName: '',
          Description:'',
          CategoryId:null,
          Category:null,
          ImagePath:''
        }
      })
  }

  setAll() {
    this.getAll().subscribe((data) => {
      this.products = data;
      this.listChanged.next(this.products.slice())
    },
      (error) => {
        this.products = [];
      })
  }

}
