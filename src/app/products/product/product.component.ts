import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Product } from 'src/app/ecommerce/shared/product';
import { ToastrService } from 'ngx-toastr';
import { ProductService } from '../shared/product.service';
import { Category } from 'src/app/ecommerce/shared/category';
import { CategoryService } from 'src/app/categories/shared/category.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @ViewChild('form', null) form: NgForm;

  categoryListChangedSubscription: Subscription;
  startedEditingSubscribe: Subscription;
  confirmSubscription: Subscription;

  editMode = false;
  editedItemId: number;
  editedItem: Product;
  categoryList: Category[] = [];
  InitialCategoryId = null;

  constructor(private productService: ProductService,
    private categoryService: CategoryService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.categoryService.setAll();
    this.categoryListChangedSubscription = this.categoryService.listChanged.subscribe((data) => {
      this.categoryList = data
    }, (error) => { 
      this.categoryList = [];
    });

    this.startedEditingSubscribe = this.productService.startedEditing.subscribe((itemId) => {
      this.editMode = true;
      this.editedItemId = itemId;
      this.editedItem = this.productService.products.find(x => x.Id == this.editedItemId);
      this.form.setValue({
        Name: this.editedItem.Name,
        BrandName: this.editedItem.BrandName,
        Description: this.editedItem.Description,
        CategoryId: this.editedItem.CategoryId,
        ImagePath: this.editedItem.ImagePath,
      })
    });

    this.confirmSubscription = this.productService.deleted.subscribe((data) => {
      if (data) {
        this.reset(this.form);
      }
    });

  }

  onSubmit(form: NgForm) {
    const Product = form.value;

    if (this.editMode) {
      this.productService.update(this.editedItemId, Product).subscribe((data) => {
        this.toastr.success('Update Successfull', 'Success');
        this.productService.setAll();
      },
        (error) => {
          this.toastr.error('Update Faild', 'Error');
        })
    } else {
      this.productService.add(Product).subscribe((data) => {
        this.toastr.success('Save Successfull', 'Success');
        this.productService.setAll();
      },
        (error) => {
          this.toastr.error('Save Faild', 'Error');
        })
    }
    this.reset(form);
  }

  reset(form?: NgForm) {
    if (form != null && form != undefined) {
      form.reset();
    } else {
      this.form.reset();
    }
    this.editMode = false;
  }

  ngOnDestroy() {
    this.startedEditingSubscribe.unsubscribe();
    this.confirmSubscription.unsubscribe();
    this.categoryListChangedSubscription.unsubscribe();
  }


}
