import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Category } from 'src/app/ecommerce/shared/category';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  rootUrl:string="http://localhost:53878/api/";

  startedEditing = new Subject<number>();
  listChanged = new Subject<Category[]>();
  deleted=new Subject<boolean>();

  categories: Category[] = [];
  category: Category;
  constructor(private http: HttpClient) { }

  add(entity: Category): Observable<Category> {
    return this.http.post<Category>(this.rootUrl + "Categories/PostCategory", entity);
  }

  update(id: number, entity: Category): Observable<any> {
    entity.Id=id;
    return this.http.post(this.rootUrl + "Categories/EditCategory", entity);
  }

  delete(id: number): Observable<Category> {
    return this.http.delete<Category>(this.rootUrl + "Categories" + "/" + id);
  }

  getById(id: number): Observable<Category> {
    return this.http.get<Category>(this.rootUrl + "Categories" + "/" + id);
  }
  getAll(): Observable<Category[]> {
    return this.http.get<Category[]>(this.rootUrl + "Categories");
  }

  setById(id: number) {
    this.getById(id).subscribe((data) => {
      this.category = data;
    },
      (error) => {
        this.category = {
          Id: null,
          Name: '',
        }
      })
  }

  setAll() {
    this.getAll().subscribe((data) => {
      this.categories = data;
      this.listChanged.next(this.categories.slice())
    },
      (error) => {
        this.categories = [];
      })
  }
}
